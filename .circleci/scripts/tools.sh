#!/bin/bash


function is_created_ecr_repo () {
  if [ $# -ne 2 ]; then
    echo "export the functions with: source tools.sh"
    echo "Sould use is_created_ecr_repo REGION REPO_NAME"
  else
    export IS_REPO_CREATED=`aws ecr describe-repositories  --region $1 | grep $2 | awk -F '/' 'NR==1{print $2}'`
  fi
}

function is_image_created () {
  if [ $# -ne 2 ]; then
    echo "export the function with: source tools.sh"
    echo "Should use is_image_created REGION REPO_NAME"
  else
    export REPO_IMG_TAG=`aws ecr list-images --region $1 --repository-name $2 | grep "imageTag" | awk -F ' ' '{print $2}'`
  fi
}

function build_docker_image () {
  if [ $# -ne 4 ]; then
    echo "export the function with: source tools.sh"
    echo "Should use build_docker_image AWS_ACCOUNT_ID  AWS_REGION REPO_NAME PATH_CIRCLECI_CONFIG"
  else
    docker build -t $1.dkr.ecr.$2.amazonaws.com/$3:latest $4
  fi
}

function push_docker_to_ecr () {
  if [ $# -ne 3 ]; then
    echo "export the function with: source tools.sh"
    echo "Should use push_docker_to_ecr AWS_ACCOUNT_ID  AWS_REGION REPO_NAME"
  else
    docker push $1.dkr.ecr.$2.amazonaws.com/$3:latest
  fi
}
