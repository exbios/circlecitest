const webpack = require('webpack')
const slsw = require('serverless-webpack')

module.exports = {
  entry: slsw.lib.entries,
  mode: 'production',
  target: 'node',
  externals: [
    /aws-sdk/ // Available on AWS Lambda
  ],
  plugins: [
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false
    }),
    new webpack.optimize.ModuleConcatenationPlugin(),
    // Elasticsearch module is very large because it includes all the API version. We only need to use v6.0.
    // In order for us to limit the size of the deployed lambda code we want to just include in the bundle
    // the code used for v6.0.
    // This is an hack that tells webpack not to include all the api files excluding 6_0.
    // If any problem happens with the execution of the deployed code, feel free to comment out this line.
    // The only drawback is the size of the lambda code will be much larger.
    new webpack.IgnorePlugin(/^\.\/(?!6_0)(?!0_90)\d+_[x\d]/)
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [/node_modules/, /test/],
        loader: 'babel-loader',
        query: {
          plugins: ['lodash', 'transform-decorators-legacy'],
          presets: [
            [
              'env',
              {
                target: { node: 10.15 }, // Node version on AWS Lambda
                useBuiltIns: true,
                modules: false,
                loose: true
              }
            ]
          ]
        }
      }
    ]
  }
}
