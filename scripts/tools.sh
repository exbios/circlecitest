#!/bin/bash

function is_created_ecr_repo () {
  if [ $# -ne 3 ]; then
    echo "export the functions with: source tools.sh"
    echo "Sould use is_created_ecr_repo REGION REPO_NAME"
  else
    export IS_REPO_CREATED=`aws ecr describe-repositories  --region $1 --profile mtemp | grep $2 | awk -F '/' 'NR==1{print $2}'`
  fi
}

function is_image_created () {
  if [ $# -ne 3 ]; then
    echo "export the function with: source tools.sh"
    echo "Should use is_image_created REGION REPO_NAME"
  else
    export REPO_IMG_TAG=`aws ecr list-images --region $1 --repository-name $2 | grep "imageIds" | awk -F ' ' '{print $2}'`
  fi
}

function build_docker_image () {
  if [ $# -ne 1 ]; then
    echo "export the function with: source tools.sh"
    echo "Should use build_docker_image PATH_CIRCLECI_CONFIG"
  else
    docker build -t ${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com/${STAGE}-${SS_ECR_REPO_NAME}:latest $1
  fi
}

function push_docker_to_ecr () {
  docker push ${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com/${STAGE}-${SS_ECR_REPO_NAME}:latest
}