/**
 * This is a list of all the brand names, as a set of strings.
 * Use it whenever you need an enum for brands.
 */
export const BRANDS = [
  '25beautifulhomes', // 25 Beautiful Homes
  'ab', // American Baby
  'acmecorporation', // ACME Corporation
  'ag', // Agriculture Integration
  'allyou', // All You
  'alr', // All Recipes Magazine
  'alrcom', // All Recipes
  'alrap', // All Recipes Southeast Asia
  'alrau', // All Recipes All Recipes Australia & New Zealand
  'alrap', // All Recipes Asia Pacific
  'alrar', // All Recipes Argentina
  'alrbr', // All Recipes Brazil
  'alrcn', // All Recipes China
  'alrde', // All Recipes Germany
  'alrfr', // All Recipes France
  'alrin', // All Recipes India
  'alrit', // All Recipes Italy
  'alrjp', // All Recipes Japan
  'alrkr', // All Recipes Korea
  'alrmx', // All Recipes Mexico
  'alrnl', // All Recipes Netherlands
  'alrpl', // All Recipes Poland
  'alrqc', // All Recipes Quebec
  'alrru', // All Recipes Russia,
  'alruk', // All Recipes UK & Ireland
  'amateurgardening', // Amateur Gardening
  'amateurphotographer', // Amateur Photographer
  'anglersmail', // Angler's Mail
  'apq', // All People Quilt
  'betterrecipes', // Better Recipes
  'bhg', // Better Homes And Gardens
  'bng', // Bhg Nature Garden
  'btv', // Better Tv
  'business2.0', // Business 2.0
  'celebsnow', // Celebs Now
  'ch', // Country Home
  'chat', // Chat
  'chatitsfate', // Chat It's Fate
  'chatpassion', // Chat Passion
  'child', // Child
  'chg', // Country Gardens
  'coastalliving', // Coastal Living
  'cookinglight', // Cooking Light
  'countryhomesandinteriors', // Country Homes And Interiors
  'countrylife', // Country Life
  'cozi', // Cozi
  'crafttube', // Craft Tube
  'cyclingweekly', // Cycling Weekly
  'dailycut', // Daily Cut
  'dc', // Divine Caroline
  'decanter', // Decanter
  'departures', // Departures
  'di', // Decorating Inspiration
  'diy', // Diy Advice
  'diyideas', // Do It Yourself
  'dlv', // Diabetic Living
  'equoevents', // Equo Events
  'essence', // Essence
  'etg', // Eating Well
  'ew', // Entertainment Weekly
  'extracrispy', // Extra Crispy
  'fc', // Family Circle
  'fcap', // FCAP
  'ffn', // Family Fun Magazine
  'fitness', // Fitness Magazine
  'fitwell', // Fit & Well
  'foodandwine', // Food and Wine
  'fortune', // Fortune
  'fortunesmallbusiness', // Fortune Small Business
  'fpg', // Fit Pregnancy
  'fwx', // FWx
  'golf', // Golf
  'golfmonthlyuk', // Golf Monthly
  'golfplus', // Golf Plus
  'goodtoknow', // Good To Know
  'health', // Health
  'healthline', // Healthline
  'hellogiggles', // Hello Giggles
  'hho', // Heart Healthy Online
  'homesgardens', // Homes & Gardens
  'horseandhound', // Horse And Hound
  'housetohome', // House To Home
  'hts', // How To Sew
  'ibinews', // International Boat Industry
  'idealhome', // Ideal Home
  'instant', // Instant
  'instyle', // InStyle
  'instyleuk', // InStyle UK
  'kbi', // Kitchen Bath Ideas
  'lcl', // Living The Country Life
  'lhj', // Ladies Home Journal
  'life', // Life Magazine
  'lifedeathprizes', // Life Death Prizes
  'listforlife', // List For Life
  'livesmart', // Live Smart
  'livingetc', // Livingetc
  'lookuk', // Look
  'mag', // MagazineStore
  'marieclaire', // Marie Claire
  'mgj', // The Magnolia Journal
  'mimi', // MIMI Chatter
  'mmqb', // The MMQB
  'moe', // More
  'money', // Money Magazine
  'more', // Old More
  'motorboatyachting', // Motor Boat & Yachting
  'motto', // Motto
  'mountainbikeridermbr', // Mountain Bike Rider
  'msl', // Martha Stewart Living
  'msw', // Martha Stewart Weddings
  'mwl', // Midwest Living
  'myrecipes', // My Recipes
  'myw', // My Wedding
  'nme', // NME
  'parentingcom', // Parenting
  'parents', // Parents
  'people', // People
  'peopleenespanol', // People En Español
  'peoplestyle', // People Style
  'pickmeup', // Pick Me Up
  'pickmeupspecial', // Pick Me Up Special
  'pml', // Parents Latina
  'posh', // Posh Coloring Studio
  'practicalboatowner', // Practical Boat Owner
  'realsimple', // Real Simple
  'recipecom', // Recipe
  'remodel', // Remodeling Center
  'rmm', // ReadyMade
  'rrmag', // RachaelRayMagazine
  'rugbyworld', // Rugby World
  'sbe', // Scrapbooks Etc
  'sfg', // Successful Farming
  'shootinggazette', // Shooting Gazette
  'shootingtimes', // Shooting Times
  'shootinguk', // ShootingUK
  'shp', // Shape
  'soaplife', // Soap Life
  'southernliving', // Southern Living
  'sportinggun', // Sporting Gun
  'si', // Sports Illustrated
  'sikids', // Sports Illustrated Kids
  'siplay', // Sports Illustrated Play
  'siswimsuit', // Sports Illustrated Swimsuit
  'styleathome', // Style At Home
  'sunset', // Sunset
  'superyachtbusiness', // Superyacht Business
  'superyachtworld', // Superyacht World
  'testbrand', // Test Brand
  'th', // Traditional Home
  'thedrive', // The Drive
  'thefield', // the Field
  'theknittingnetwork', // The Knitting Network
  'themondaymorningquarterback', // Monday Morning Quarterback
  'thevideomode', // The Video Mode
  'thisoldhouse', // This Old House
  'time', // Time
  'timeforkids', // Time For Kids
  'tm', // Travel Meredith Integration
  'travelandleisure', // Travel & Leisure
  'trustedreviews', // Trusted Review
  'tvsatelliteweek', // TV & Satellite Week
  'tvtimes', // TV Times
  'uncut', // Uncut
  'visitcalifornia', // Visit California
  'volksworld', // VolksWorld
  'vwcamperbus', // VW Camper & Bus
  'vwt', // VWt
  'wallpaper', // Wallpaper
  'weawe', // weAWE
  'whatdigitalcamera', // What Digital Camera
  'whatsontv', // Whats on TV
  'woman', // Woman
  'womanhome', // Woman & Home
  'womanhomefeelgoodfood', // Woman & Home Feel Good Food
  'womanhomefeelgoodyou', // Woman & Home Feel Good You
  'womansown', // Woman's Own
  'womansownlifestyleseries', // Woman's Own Lifestyle Series
  'womanspecialseries', // Woman Special Series
  'womansweekly', // Woman's Weekly
  'womansweeklyfictionspecial', // Woman's Weekly Fiction Special
  'womanssweeklyknittingcraft', // Womans Knitting Craft
  'womansweeklylivingseries', // Woman's Weekly Living Series
  'wood', // Wood
  'worldsoccer', // World Soccer
  'xojane', // xoJane
  'yachtingboatingworld', // Yachting & Boating World
  'yachtingmonthly', // Yachting Monthly
  'yachtingworld' // Yachting World
]

export const OBJECT_TYPE = {
  NEWSLETTER: 'newsletter',
  ALERT: 'alert'
}

export const OWNER = {
  DIGITAL: 'digital',
  CRT: 'crt'
}

export const REGISTRATION_SOURCE_TYPE = {
  ONSITE: 'onsite',
  OFFSITE: 'offsite',
  INDIRECT: 'indirect'
}

export const END_OF_PROCESS_TYPE = {
  EO: 'EO',
  CUSTOM_URL: 'Custom URL',
  APP: 'App',
  PDF: 'PDF',
  ODDCAST: 'Oddcast',
  ZIP: 'Zip'
}

export const DEFAULT_PROVIDER = 'Meredith'
