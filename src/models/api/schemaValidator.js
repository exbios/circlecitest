import Ajv from 'ajv'
import loggerFactory from '../../lib/services/loggerFactory'
import { failure400 } from '../../lib/apigLambdaProxyHelper'

const logger = loggerFactory('SchemaValidator')
const ajv = new Ajv({ allErrors: true })

export function isValidSchema(schema, object) {
  let validPayload
  try {
    validPayload = JSON.parse(object)
  } catch (jsonError) {
    throw failure400(`Error parsing JSON ${jsonError}`)
  }

  const test = ajv.compile(schema)
  const isValid = test(validPayload)
  if (isValid) {
    return true
  } else {
    logger.info('In Schema validator:', JSON.stringify({ validPayload, error: test.errors }))
    throw failure400(test.errors)
  }
}
