import baseMessages, { getErrorObj, getFormattedMessage } from '../lib/messagesStore'

const messages = Object.assign({}, baseMessages, {
  ERR_STRATEGY_NOT_FOUND: {
    name: 'StrategyNotFoundError',
    message: 'Strategy not found'
  },
  ERR_MISSING_STRATEGY_DISPATCH: {
    name: 'MissingStrategyDispatchError',
    message: 'Missing dispatch() methods on strategy instance'
  },
  ERR_FAILED_TO_EXTRACT_BODY: {
    name: 'FailedToExtractBody',
    message: 'Failed to extract event body'
  },
  ERR_MISSING_EVENT_BODY: {
    name: 'ErrMissingEventBody',
    message: 'Missing event body'
  },
  ERR_FAILED_TO_CREATE_REGSOURCE: {
    name: 'FailedCreatingRegSource',
    message: 'Failed to create registration source: %s'
  },
  ERR_MISSING_PATH_PARAMS: {
    name: 'ErrMissingPathParams',
    message: 'Missing path params'
  },
  ERR_MISSING_PARAMETER_PATH: {
    name: 'ErrMissingParameterPath',
    message: 'Missing parameters in the path'
  },
  ERR_MISSING_REGSOURCE_ID: {
    name: 'ErrMissingRegSourceId',
    message: 'Request missing registration source id.'
  },
  ERR_MISSING_NEWSLETTER_ID: {
    name: 'ErrMissingNewsletterId',
    message: 'Request missing newsletter id.'
  },
  ERR_BAD_QUERY_PARAMETER: {
    name: 'ErrBadQueryParameter',
    message: 'Invalid query parameter was sent.'
  }
})

export default messages
export { getErrorObj, getFormattedMessage }
