import { isFunction } from 'lodash'
import { Container } from 'aurelia-dependency-injection'
import loggerFactory from '../../lib/services/loggerFactory'
import messages, { getErrorObj } from '../../messagesStore'
import routes from '../../handlers/router/routes.json'

const logger = loggerFactory('DispatcherService')

export default class DispatcherService {
  constructor() {
    this.strategies = {}
    this._initializeStrategies()
  }

  async dispatch(route, event, context) {
    if (!this.strategies[`${route}`]) {
      logger.info(`Dispatch strategy not found with route "${route}"`)
      throw getErrorObj(messages.ERR_STRATEGY_NOT_FOUND)
    }
    logger.debug(`Dispatching with route "${route}" and strategy "${this.strategies[`${route}`].constructor.name}":`)
    return this.strategies[`${route}`].dispatch(event, context)
  }

  setStrategy(route, strategy) {
    if (!isFunction(strategy.dispatch)) {
      throw getErrorObj(messages.ERR_MISSING_STRATEGY_DISPATCH)
    }
    this.strategies[`${route}`] = strategy
    logger.debug(`Added new strategy with key "${route}"`, strategy)
  }

  _initializeStrategies() {
    Object.entries(routes).forEach(([key, value]) => {
      logger.debug('initializing', `${key}`, 'strategy', value.strategy)
      this.setStrategy(key, Container.instance.get(value.strategy))
    })
  }
}
