import 'babel-polyfill'
import 'aurelia-polyfills'
import { Container } from 'aurelia-dependency-injection'
import DispatcherService from './DispatcherService'
import messages from '../../messagesStore'
import routes from '../../handlers/router/routes.json'

describe('DispatcherService', () => {
  let service, container

  beforeEach(() => {
    container = new Container()
    container.makeGlobal()

    Object.entries(routes).forEach(([key, value]) => {
      container.registerSingleton(value.strategy, require('./strategies/' + value.strategy).default)
    })
    service = new DispatcherService()
  })

  it('should initialize with proper strategies', () => {
    const strategiesCount = Object.keys(routes).length
    expect(Object.keys(service.strategies).length).toEqual(strategiesCount)
    Object.entries(routes).forEach(([key, value]) => {
      expect(service.strategies[`${key}`].constructor.name).toEqual(value.strategy)
    })
  })

  describe('when setStrategy() is called', () => {
    it('should add passed strategy to internal strategies object', () => {
      const expectedStrategy = {
        dispatch: () => {}
      }
      service.setStrategy('POST::/resource/path', expectedStrategy)
      expect(service.strategies[`POST::/resource/path`]).toEqual(expectedStrategy)
    })

    it('should throw if passed strategy object does not have a dispatch function', () => {
      try {
        service.setStrategy('PUT::/resource/path', { prop1: 'prop1' })
        throw new Error('You should not be here')
      } catch (err) {
        expect(err.name).toEqual(messages.ERR_MISSING_STRATEGY_DISPATCH.name)
        expect(err.message).toEqual(messages.ERR_MISSING_STRATEGY_DISPATCH.message)
      }
    })
  })

  describe('when dispatch() is executed', () => {
    it('should dispatch with mapped strategy and passed arguments', async () => {
      const dispatchSpy = jest.fn(() => Promise.resolve())
      service.setStrategy('GET::/resource/path', {
        dispatch: dispatchSpy
      })
      const expectedArgs = { prop1: 'arg_1' }
      const expectedContext = { propCtx1: 'arg_ctx_1' }
      await service.dispatch('GET::/resource/path', expectedArgs, expectedContext)
      expect(dispatchSpy).toHaveBeenCalledWith(expectedArgs, expectedContext)
      expect(dispatchSpy.mock.calls.length).toEqual(1)
    })

    it('should throw if path is not mapped to a strategy', async () => {
      service.setStrategy('GET::/other/resource/path', {
        dispatch: () => Promise.resolve()
      })
      try {
        await service.dispatch('GET::/resource/path')
        throw new Error('You should not be here')
      } catch (err) {
        expect(err.name).toEqual(messages.ERR_STRATEGY_NOT_FOUND.name)
        expect(err.message).toEqual(messages.ERR_STRATEGY_NOT_FOUND.message)
      }
    })

    it('should re-throw if dispatching fails', async () => {
      const expectedError = new Error('dispatch error')
      const dispatchSpy = jest.fn(() => Promise.reject(expectedError))
      service.setStrategy('GET::/resource/path', {
        dispatch: dispatchSpy
      })
      try {
        await service.dispatch('GET::/resource/path')
        throw new Error('should not be here')
      } catch (err) {
        expect(err).toEqual(expectedError)
      }
    })

    it('should return dispatched response', async () => {
      const expectedDispatchedResponse = { prop1: 'prop1 value' }
      const dispatchSpy = jest.fn(() => Promise.resolve(expectedDispatchedResponse))
      service.setStrategy('GET::/resource/path', {
        dispatch: dispatchSpy
      })
      const res = await service.dispatch('GET::/resource/path')
      expect(res).toEqual(expectedDispatchedResponse)
    })
  })
})
