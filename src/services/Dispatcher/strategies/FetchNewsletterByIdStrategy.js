import { inject } from 'aurelia-dependency-injection'
import { isNil } from 'lodash'
import { success, failure400, failure404, failure500 } from '../../../lib/apigLambdaProxyHelper'
import messages from '../../../messagesStore'
import loggerFactory from '../../../lib/services/loggerFactory'

const logger = loggerFactory('FetchNewsletterById')

@inject('EnvVarsManager', 'Lambda')
export default class FetchNewsletterByIdStrategy {
  constructor(envVarsManager, lambda) {
    this._envVarsManager = envVarsManager
    this._lambda = lambda
  }

  async dispatch(event) {
    logger.info(`The event includes: ${JSON.stringify(event.pathParameters)}`)
    if (isNil(event.pathParameters) || isNil(event.pathParameters.newsletterId)) {
      throw failure400(messages.ERR_MISSING_NEWSLETTER_ID)
    }

    const newsletterId = event.pathParameters.newsletterId

    const params = {
      FunctionName: this._envVarsManager.get('fetchNewsletterByIdFunction'),
      Payload: JSON.stringify({ newsletterId })
    }

    const response = await this._lambda.call('invoke', params)
    if (response && response.Payload) {
      logger.info(JSON.stringify(response))
      if (response.FunctionError) {
        const errorObj = JSON.parse(response.Payload)
        logger.error(`An error was returned to dispatcher: ${JSON.stringify(errorObj)}`)
        switch (errorObj.errorType) {
          case 'ErrFetchingNewsletter':
            throw failure404(errorObj)
          default:
            throw failure500(errorObj)
        }
      }
      return success(JSON.parse(response.Payload))
    }
  }
}
