import { inject } from 'aurelia-dependency-injection'
import { failure400, success201, failure500 } from '../../../lib/apigLambdaProxyHelper'
import { isValidSchema } from '../../../models/api/schemaValidator'
import { regSourceCreateRequest } from '../../../../documentation/models/regSource'
import messages from '../../../messagesStore'
import { DEFAULT_PROVIDER } from '../../../models/api/enumConstants'
import loggerFactory from '../../../lib/services/loggerFactory'
import { isNil } from 'lodash'

const logger = loggerFactory('CreateRegSourceStrategy')

@inject('EnvVarsManager', 'Lambda')
export default class CreateRegSourceStrategy {
  constructor(envVarsManager, lambda) {
    this._envVarsManager = envVarsManager
    this._lambda = lambda
  }
  async dispatch(event) {
    if (!event.body) {
      throw failure400(messages.ERR_MISSING_EVENT_BODY)
    }
    // use models to validate the registration source object
    logger.info('Validating the schema')
    isValidSchema(regSourceCreateRequest, event.body)

    // Set default values
    const payload = JSON.parse(event.body)
    if (isNil(payload.data.active)) {
      payload.data.active = true
    }
    if (isNil(payload.data.paidFlag)) {
      payload.data.paidFlag = false
    }
    if (isNil(payload.data.provider)) {
      payload.data.provider = DEFAULT_PROVIDER
    }

    const params = {
      FunctionName: this._envVarsManager.get('createRegSourceFunction'),
      Payload: JSON.stringify(payload)
    }

    const response = await this._lambda.call('invoke', params)
    if (response && response.Payload) {
      if (response.FunctionError) {
        const errorObj = JSON.parse(response.Payload)
        logger.error(`An error was returned to dispatcher: ${JSON.stringify(errorObj)}`)
        switch (errorObj.errorType) {
          case 'ErrCreatingRegSource':
            throw failure400(errorObj)
          default:
            throw failure500(errorObj)
        }
      }
      return success201(JSON.parse(response.Payload))
    }
  }
}
