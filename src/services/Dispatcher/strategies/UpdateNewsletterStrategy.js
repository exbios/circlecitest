import { inject } from 'aurelia-dependency-injection'
import { isValidSchema } from '../../../models/api/schemaValidator'
import { newsletterUpdateRequest } from '../../../../documentation/models/newsletters'
import messages from '../../../messagesStore'
import { failure400, failure409, failure500, success } from '../../../lib/apigLambdaProxyHelper'
import loggerFactory from '../../../lib/services/loggerFactory'
import { isNil } from 'lodash'

const logger = loggerFactory('UpdateNewsletterStrategy')

@inject('EnvVarsManager', 'Lambda')
export default class UpdateNewsletterStrategy {
  constructor(envVarsManager, lambda) {
    this._envVarsManager = envVarsManager
    this._lambda = lambda
  }
  async dispatch(event) {
    if (isNil(event.body)) {
      throw failure400(messages.ERR_MISSING_EVENT_BODY)
    }

    logger.info('Validating request schema')
    isValidSchema(newsletterUpdateRequest, event.body)

    const newsletterObj = JSON.parse(event.body).data
    newsletterObj.newsletterId = event.pathParameters.newsletterId

    const params = {
      FunctionName: this._envVarsManager.get('updateNewsletterFunction'),
      Payload: JSON.stringify(newsletterObj)
    }

    const response = await this._lambda.call('invoke', params)
    logger.debug('Response from update newsletter service: ', JSON.stringify(response))
    if (response && response.Payload) {
      if (response.FunctionError) {
        const errorObj = JSON.parse(response.Payload)
        logger.error(`An error was returned to dispatcher: ${JSON.stringify(errorObj)}`)
        switch (errorObj.errorType) {
          case 'ErrUpdatingNewsletter':
            throw failure409(errorObj)
          default:
            throw failure500(errorObj)
        }
      }
      return success(JSON.parse(response.Payload))
    }
  }
}
