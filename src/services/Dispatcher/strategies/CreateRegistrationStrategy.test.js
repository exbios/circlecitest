import CreateRegistrationStrategy from './CreateRegistrationStrategy'

describe('CreateRegistrationStrategy', () => {
  let strategy, mockEnvVars, mockLambda
  beforeEach(() => {
    mockEnvVars = {
      get: jest.fn(key => {
        if (key === 'fetchHashIdFunction') return 'fetchHashIdFunction'
      })
    }
    mockLambda = {
      call: jest.fn(() => Promise.resolve())
    }
    strategy = new CreateRegistrationStrategy(mockEnvVars, mockLambda)
  })
  describe('when valid body was send it', () => {
    let event, validRegReqBody
    beforeEach(() => {
      event = {}
      validRegReqBody = {
        data: {
          regSourceId: 'zz99pq',
          email: 'test@email.com',
          brand: 'bhg',
          actions: [
            {
              subscribe: 0,
              objectName: 'testObj'
            }
          ]
        }
      }
      event.body = JSON.stringify(validRegReqBody)
      mockLambda.call.mockReturnValueOnce({ Payload: JSON.stringify({ data: { hash_id: '239871049' } }) })
    })
    it('should call envVars and Lambda', async () => {
      await strategy.dispatch(event)
      expect(mockEnvVars.get).toHaveBeenCalled()
      expect(mockEnvVars.get).toHaveBeenCalledWith('fetchHashIdFunction')
      expect(mockLambda.call).toHaveBeenCalled()
      expect(mockLambda.call.mock.calls[0][0]).toEqual('invoke')
      expect(mockLambda.call.mock.calls[0][1].Payload).toEqual(JSON.stringify(validRegReqBody))
    })
    it('should return status 200', async () => {
      const response = await strategy.dispatch(event)
      expect(response.statusCode).toEqual(200)
      expect(JSON.parse(response.body).data.hash_id).toBeDefined()
    })
  })
  describe('when an invalid request is sent', () => {
    let event, badRequest
    beforeEach(() => {
      event = {}
      badRequest = {
        data: {
          regSourceId: 'zz99pq',
          email: 'test@email.com',
          brand: 'bhg',
          actions: [
            {
              subscribe: 0,
              objectName: 'testObj',
              wrong: 'should not be valid'
            }
          ]
        }
      }
      event.body = JSON.stringify(badRequest)
    })
    it('should return a validation error', async () => {
      try {
        await strategy.dispatch(event)
        throw new Error('Should not return this error')
      } catch (err) {
        expect(err.statusCode).toEqual(400)
      }
    })
  })
  describe('when the lambda returns an error', () => {
    let event, validRegReqBody
    beforeEach(() => {
      event = {}
      validRegReqBody = {
        data: {
          regSourceId: 'zz99pq',
          email: 'test@email.com',
          brand: 'bhg',
          actions: [
            {
              subscribe: 0,
              objectName: 'testObj'
            }
          ]
        }
      }
      let expectedError = {
        StatusCode: 200,
        FunctionError: 'Handled',
        ExecutedVersion: '$LATEST',
        Payload: '{"errorMessage":"Error","errorType":"SomeErrorFromLambda","stackTrace":["e.getErrorObj (/var//index.js:1:123)","E.t.(anonymous function)"]}'
      }
      event.body = JSON.stringify(validRegReqBody)
      mockLambda.call.mockReturnValueOnce(expectedError)
    })
    it('should return a 400 error', async () => {
      try {
        await strategy.dispatch(event)
      } catch (err) {
        expect(err).toBeDefined()
        expect(err.statusCode).toEqual(400)
      }
    })
  })
})
