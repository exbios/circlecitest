import UpdateRegSourceStrategy from './UpdateRegSourceStrategy'
import { BRANDS, REGISTRATION_SOURCE_TYPE, END_OF_PROCESS_TYPE } from '../../../models/api/enumConstants'

describe('UpdateRegSourceStrategy', () => {
  let strategy, event, mockEnvVars, mockLambda, expectedRegSourceFunction
  strategy = new UpdateRegSourceStrategy()
  beforeEach(() => {
    expectedRegSourceFunction = 'updateRegSource'
    mockEnvVars = {
      get: jest.fn(key => {
        if (key === 'updateRegSourceFunction') return expectedRegSourceFunction
      })
    }
    mockLambda = {
      call: jest.fn(() => Promise.resolve())
    }
    strategy = new UpdateRegSourceStrategy(mockEnvVars, mockLambda)
  })
  describe('when dispatch() is called', () => {
    beforeEach(() => {
      event = {}
      event.pathParameters = {
        regsourceId: 'bb99yh'
      }
      event.body = JSON.stringify({
        data: {
          name: 'Registration Source 1',
          brand: BRANDS[22],
          registrationSourceType: REGISTRATION_SOURCE_TYPE.ONSITE,
          endOfProcessType: END_OF_PROCESS_TYPE.PDF
        }
      })
    })
    it('should invoke the correct lambda', async () => {
      await strategy.dispatch(event)
      expect(mockEnvVars.get).toHaveBeenCalled()
      expect(mockEnvVars.get).toHaveBeenCalledWith('updateRegSourceFunction')
      expect(mockLambda.call).toHaveBeenCalled()
      expect(mockLambda.call.mock.calls[0][0]).toEqual('invoke')
      expect(mockLambda.call.mock.calls[0][1].FunctionName).toEqual(expectedRegSourceFunction)
    })
  })
  describe('when event is passed with a body', () => {
    beforeEach(() => {
      event = {}
      event.pathParameters = {
        regsourceId: 'bb99yh'
      }
      event.body = JSON.stringify({
        data: {
          name: 'Registration Source 1',
          brand: BRANDS[22],
          registrationSourceType: REGISTRATION_SOURCE_TYPE.ONSITE,
          endOfProcessType: END_OF_PROCESS_TYPE.PDF
        }
      })
      mockLambda.call.mockReturnValueOnce({
        Payload: JSON.stringify({ data: event.body })
      })
    })
    it('should return 200 response', async () => {
      const response = await strategy.dispatch(event)
      expect(response).toBeDefined()
      expect(response.statusCode).toEqual(200)
      expect(response.body).toBeDefined()
    })
  })
  describe('when event is passed without a body', () => {
    beforeEach(() => {
      event = {}
    })
    it('should return 400 statusCode', async () => {
      try {
        await strategy.dispatch(event)
      } catch (err) {
        expect(err).toBeDefined()
        expect(err.statusCode).toEqual(400)
      }
    })
  })
  describe('when event does not include a name', () => {
    event = {}
    event.pathParameters = {
      regsourceId: 'bb99yh'
    }
    event.body = JSON.stringify({
      data: {
        brand: 'bhg',
        registrationSourceType: 'onsite'
      }
    })
    it('should return a 400 error', async () => {
      try {
        await strategy.dispatch(event)
      } catch (err) {
        expect(err).toBeDefined()
        expect(err.statusCode).toEqual(400)
      }
    })
  })
  describe('when the lambda returns an error', () => {
    beforeEach(() => {
      event = {}
      event.pathParameters = {
        regsourceId: 'bb99yh'
      }
      event.body = JSON.stringify({
        data: {
          name: 'Registration Source 1',
          brand: BRANDS[22],
          registrationSourceType: REGISTRATION_SOURCE_TYPE.ONSITE,
          endOfProcessType: END_OF_PROCESS_TYPE.PDF
        }
      })
      let expectedError = {
        StatusCode: 200,
        FunctionError: 'Handled',
        ExecutedVersion: '$LATEST',
        Payload: '{"errorMessage":"Error","errorType":"ErrUpdatingRegSource","stackTrace":["e.getErrorObj (/var//index.js:1:123)","E.t.(anonymous function)"]}'
      }
      mockLambda.call.mockReturnValueOnce(expectedError)
    })
    it('should return a 409 error', async () => {
      try {
        await strategy.dispatch(event)
      } catch (err) {
        expect(err).toBeDefined()
        expect(err.statusCode).toEqual(409)
      }
    })
  })
  describe('when the lambda returns another error', () => {
    beforeEach(() => {
      event = {}
      event.pathParameters = {
        regsourceId: 'bb99yh'
      }
      event.body = JSON.stringify({
        data: {
          name: 'Registration Source 1',
          brand: BRANDS[22],
          registrationSourceType: REGISTRATION_SOURCE_TYPE.ONSITE,
          endOfProcessType: END_OF_PROCESS_TYPE.PDF
        }
      })
      let expectedError = {
        StatusCode: 200,
        FunctionError: 'Handled',
        ExecutedVersion: '$LATEST',
        Payload: '{"errorMessage":"Error","errorType":"InternalServerError","stackTrace":["e.getErrorObj (/var//index.js:1:123)","E.t.(anonymous function)"]}'
      }
      mockLambda.call.mockReturnValueOnce(expectedError)
    })
    it('should return a 500 error', async () => {
      try {
        await strategy.dispatch(event)
      } catch (err) {
        expect(err).toBeDefined()
        expect(err.statusCode).toEqual(500)
      }
    })
  })
})
