import DeleteNewsletterStrategy from './DeleteNewsletterStrategy'

describe('DeleteNewsletterStrategy', () => {
  let strategy, event, mockEnvVars, mockLambda, expectedNewsletterFunction
  strategy = new DeleteNewsletterStrategy()
  beforeEach(() => {
    expectedNewsletterFunction = 'deleteNewsletter'
    mockEnvVars = {
      get: jest.fn(key => {
        if (key === 'deleteNewsletterFunction') return expectedNewsletterFunction
      })
    }
    mockLambda = {
      call: jest.fn(() => Promise.resolve())
    }
    strategy = new DeleteNewsletterStrategy(mockEnvVars, mockLambda)
  })
  describe('when dispatch() is called', () => {
    beforeEach(() => {
      event = {
        pathParameters: { newsletterId: '0c1f19' }
      }
    })
    it('should invoke the correct lambda', async () => {
      await strategy.dispatch(event)
      expect(mockEnvVars.get).toHaveBeenCalledWith('deleteNewsletterFunction')
      expect(mockLambda.call).toHaveBeenCalled()
      expect(mockLambda.call.mock.calls[0][0]).toEqual('invoke')
      expect(mockLambda.call.mock.calls[0][1].FunctionName).toEqual(expectedNewsletterFunction)
    })
  })
  describe('when an existing newsletter id is passed', () => {
    beforeEach(() => {
      event = {
        pathParameters: {newsletterId: '0c1f19'}
      }
      mockLambda.call.mockReturnValueOnce({
        Payload: JSON.stringify({})
      })
    })
    it('should return 204 response', async () => {
      const response = await strategy.dispatch(event)
      expect(response.body).toEqual(JSON.stringify({}))
      expect(response.statusCode).toEqual(204)
    })
  })
  describe('when event is passed without a path parameter', () => {
    beforeEach(() => {
      event = {
        pathParameters: {newsletterId: null}
      }
    })
    it('should return 400 statusCode', async () => {
      try {
        await strategy.dispatch(event)
      } catch (err) {
        var errorBody = JSON.parse(err.body)
        expect(err).toBeDefined()
        expect(err.statusCode).toEqual(400)
        expect(errorBody.errors[0].source.context.message).toEqual('Missing path params')
      }
    })
  })
  describe('when delete service returns an error', () => {
    beforeEach(() => {
      event = {
        pathParameters: {newsletterId: '0c1f19'}
      }
      let expectedError = {
        StatusCode: 200,
        FunctionError: 'Handled',
        ExecutedVersion: '$LATEST',
        Payload: '{"errorMessage":"Error updating: Error: ConditionalCheckFailedException:","errorType":"ErrDeletingNewsletter","stackTrace":["n.getErrorObj"]}'
      }
      mockLambda.call.mockReturnValueOnce(expectedError)
    })
    it('should return a 409 Conflict error', async () => {
      try {
        await strategy.dispatch(event)
      } catch (err) {
        expect(err).toBeDefined()
        expect(err.statusCode).toEqual(409)
      }
    })
  })
  describe('when delete service returns an unexpected error', () => {
    beforeEach(() => {
      event = {
        pathParameters: {newsletterId: '0c1f19'}
      }
      let expectedError = {
        StatusCode: 200,
        FunctionError: 'Handled',
        ExecutedVersion: '$LATEST',
        Payload: '{"errorMessage":"Error","errorType":"InternalServerError","stackTrace":["e.getErrorObj (/var//index.js:1:123)","E.t.(anonymous function)"]}'
      }
      mockLambda.call.mockReturnValueOnce(expectedError)
    })
    it('should return a 500 Internal Server Error', async () => {
      try {
        await strategy.dispatch(event)
      } catch (err) {
        expect(err).toBeDefined()
        expect(err.statusCode).toEqual(500)
      }
    })
  })
})
