import { isNil } from 'lodash'
import { inject } from 'aurelia-dependency-injection'
import { failure400, success } from '../../../lib/apigLambdaProxyHelper'
import messages from '../../../messagesStore'
import loggerFactory from '../../../lib/services/loggerFactory'
import { isValidSchema } from '../../../models/api/schemaValidator'
import { registrationRequest } from '../../../../documentation/models/registration'

const logger = loggerFactory('CreateNewsletterStrategy')

@inject('EnvVarsManager', 'Lambda')
export default class CreateRegistrationStrategy {
  constructor(envVarsManager, lambda) {
    this._envVarsManager = envVarsManager
    this._lambda = lambda
  }
  async dispatch(event) {
    if (isNil(event.body)) {
      throw failure400(messages.ERR_MISSING_EVENT_BODY)
    }
    logger.info('Validating request schema.')
    isValidSchema(registrationRequest, event.body)

    const params = {
      FunctionName: this._envVarsManager.get('fetchHashIdFunction'),
      Payload: event.body
    }

    const response = await this._lambda.call('invoke', params)
    if (response && response.Payload) {
      if (response.FunctionError) {
        const errorObj = JSON.parse(response.Payload)
        throw failure400(errorObj)
      }
      return success(JSON.parse(response.Payload))
    }
  }
}
