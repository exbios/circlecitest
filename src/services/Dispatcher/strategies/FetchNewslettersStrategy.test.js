import FetchNewslettersStrategy from './FetchNewslettersStrategy'
import {OWNER, OBJECT_TYPE, BRANDS} from '../../../models/api/enumConstants'

describe('FetchNewslettersStrategy', () => {
  let strategy, mockEnvVars, mockLambda, expectedNewsletterFunction, expectedEvent
  beforeEach(() => {
    expectedNewsletterFunction = 'fetchAllNewslettersFunction'
    expectedEvent = {
      resource: '/newsletters',
      path: '/newsletters',
      httpMethod: 'GET',
      queryStringParameters: {
        brandId: 'cols'
      },
      pathParameters: null,
      stageVariables: null,
      body: null
    }
    mockEnvVars = {
      get: jest.fn(key => {
        if (key === 'fetchAllNewslettersFunction') return expectedNewsletterFunction
      })
    }
    mockLambda = {
      call: jest.fn(() => Promise.resolve())
    }
    strategy = new FetchNewslettersStrategy(mockEnvVars, mockLambda)
  })

  describe('when handler executed correctly', () => {
    let expectedResponse
    let emptyEvent = {}
    beforeEach(() => {
      expectedResponse = {
        Payload: {
          data: [{
            title: 'Preparing for Pregnancy',
            description: 'Are you thinking of getting pregnant or already trying to conceive?',
            imageUrl: '/parents/images/newsletter/PFP_NLPage.jpg',
            brand: BRANDS[28],
            objectType: OBJECT_TYPE.NEWSLETTER,
            owner: OWNER.CRT,
            active: true,
            tags: ['Holidays']
          }]
        }
      }
      mockLambda.call.mockReturnValueOnce({ Payload: JSON.stringify(expectedResponse.Payload) })
    })
    it('should return a 200 success', async () => {
      const response = await strategy.dispatch(emptyEvent)
      expect(response).toBeDefined()
      expect(response.statusCode).toEqual(200)
      expect(response.body).toBeDefined()
    })
  })

  describe('when dispatch() is called', () => {
    it('should invoke the correct lambda', async () => {
      await strategy.dispatch(expectedEvent)
      expect(mockEnvVars.get).toHaveBeenCalled()
      expect(mockEnvVars.get).toHaveBeenCalledWith(expectedNewsletterFunction)
      expect(mockLambda.call).toHaveBeenCalled()
      expect(mockLambda.call.mock.calls[0][0]).toEqual('invoke')
      expect(mockLambda.call.mock.calls[0][1].FunctionName).toEqual(expectedNewsletterFunction)
      expect(mockLambda.call.mock.calls[0][1].Payload).toEqual(JSON.stringify(expectedEvent.queryStringParameters))
    })
  })

  describe('when called with missing or bad queryParameter', () => {
    let event
    beforeEach(() => {
      event = {
        queryStringParameters: {
          brand: 'xxxx'
        }
      }
    })
    it('should return a 400 error', async () => {
      try {
        await strategy.dispatch(event)
        throw new Error('Should not hit this error')
      } catch (err) {
        expect(err).toBeDefined()
        expect(err.statusCode).toEqual(400)
      }
    })
  })

  describe('when the lambda returns an error', () => {
    beforeEach(() => {
      let expectedError = {
        StatusCode: 200,
        FunctionError: 'Handled',
        ExecutedVersion: '$LATEST',
        Payload: '{"errorMessage":"Error","errorType":"InternalServerError ","stackTrace":["e.getErrorObj (/var//index.js:1:123)","E.t.(anonymous function)"]}'
      }
      mockLambda.call.mockReturnValueOnce(expectedError)
    })
    it('should return a 500 error', async () => {
      try {
        await strategy.dispatch(expectedEvent)
        throw new Error('Should not hit this error')
      } catch (err) {
        expect(err).toBeDefined()
        expect(err.statusCode).toEqual(500)
      }
    })
  })
})
