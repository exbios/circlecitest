import { inject } from 'aurelia-dependency-injection'
import { isNil } from 'lodash'
import { success, failure400, failure404 } from '../../../lib/apigLambdaProxyHelper'
import messages from '../../../messagesStore'
import loggerFactory from '../../../lib/services/loggerFactory'

const logger = loggerFactory('FetchRegSourceById')

@inject('EnvVarsManager', 'Lambda')
export default class FetchRegSourceByIdStrategy {
  constructor(envVarsManager, lambda) {
    this._envVarsManager = envVarsManager
    this._lambda = lambda
  }

  async dispatch(event) {
    logger.info(`The event includes: ${JSON.stringify(event.pathParameters)}`)
    if (isNil(event.pathParameters) || isNil(event.pathParameters.regsourceId)) {
      throw failure400(messages.ERR_MISSING_REGSOURCE_ID)
    }

    const regsourceId = event.pathParameters.regsourceId

    const params = {
      FunctionName: this._envVarsManager.get('fetchRegSourceByIdFunction'),
      Payload: JSON.stringify({ regsourceId })
    }

    const response = await this._lambda.call('invoke', params)
    if (response && response.Payload) {
      logger.info(JSON.stringify(response))
      if (response.FunctionError) {
        const errorObj = JSON.parse(response.Payload)
        logger.error(`An error was returned to dispatcher: ${JSON.stringify(errorObj)}`)
        throw failure404(errorObj.errorMessage)
      }
      return success(JSON.parse(response.Payload))
    }
  }
}
