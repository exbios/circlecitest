import { inject } from 'aurelia-dependency-injection'
import { failure400, failure409, failure500, success204 } from '../../../lib/apigLambdaProxyHelper'
import loggerFactory from '../../../lib/services/loggerFactory'
import { isNil } from 'lodash'
import messages from '../../../messagesStore'

const logger = loggerFactory('DeleteRegSourceStrategy')

@inject('EnvVarsManager', 'Lambda')
export default class DeleteRegSourceStrategy {
  constructor(envVarsManager, lambda) {
    this._envVarsManager = envVarsManager
    this._lambda = lambda
  }
  async dispatch(event) {
    logger.debug('event', event)
    if (isNil(event.pathParameters.regsourceId)) {
      throw failure400(messages.ERR_MISSING_PATH_PARAMS)
    }
    const params = {
      FunctionName: this._envVarsManager.get('deleteRegSourceFunction'),
      Payload: JSON.stringify(event.pathParameters.regsourceId)
    }
    const response = await this._lambda.call('invoke', params)
    logger.debug('Response from service: ', JSON.stringify(response))
    if (response && response.Payload) {
      if (response.FunctionError) {
        const errorObj = JSON.parse(response.Payload)
        logger.error(`An error was returned to dispatcher: ${JSON.stringify(errorObj)}`)
        switch (errorObj.errorType) {
          case 'ErrDeletingRegSource':
            throw failure409(errorObj)
          default:
            throw failure500(errorObj)
        }
      }
      return success204()
    }
  }
}
