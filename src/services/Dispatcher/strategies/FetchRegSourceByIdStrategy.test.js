import FetchRegSourceByIdStrategy from './FetchRegSourceByIdStrategy'

describe('FetchRegSourceByIdStrategy', () => {
  let strategy, event, mockEnvVars, mockLambda, expectedFunction
  strategy = new FetchRegSourceByIdStrategy()
  beforeEach(() => {
    expectedFunction = 'fetchRegSourceByIdFunction'
    mockEnvVars = {
      get: jest.fn(key => {
        if (key === 'fetchRegSourceByIdFunction') return expectedFunction
      })
    }
    mockLambda = {
      call: jest.fn(() => Promise.resolve())
    }
    strategy = new FetchRegSourceByIdStrategy(mockEnvVars, mockLambda)
  })
  describe('when dispatch() is called', () => {
    beforeEach(() => {
      event = {}
      event.pathParameters = {
        regsourceId: 'bb99yh'
      }
    })
    it('should make the correct calls', async () => {
      await strategy.dispatch(event)
      expect(mockEnvVars.get).toHaveBeenCalled()
      expect(mockEnvVars.get).toHaveBeenCalledWith('fetchRegSourceByIdFunction')
      expect(mockLambda.call).toHaveBeenCalled()
      expect(mockLambda.call.mock.calls[0][0]).toEqual('invoke')
      expect(mockLambda.call.mock.calls[0][1].FunctionName).toEqual(expectedFunction)
    })
  })
  describe('when a valid regsource Id is passed', () => {
    beforeEach(() => {
      event = {}
      event.pathParameters = {
        regsourceId: 'bb99yh'
      }
      mockLambda.call.mockReturnValueOnce({ Payload: JSON.stringify({ data: {} }) })
    })
    it('should return 200 response', async () => {
      const response = await strategy.dispatch(event)
      expect(response.statusCode).toEqual(200)
      expect(response.body).toBeDefined()
    })
  })
  describe('when an invalid regsource Id is passed', () => {
    beforeEach(() => {
      event = {}
      event.pathParameters = {
        regsourceId: 'bb99yh'
      }
      let expectedError = {
        StatusCode: 200,
        FunctionError: 'Handled',
        ExecutedVersion: '$LATEST',
        Payload: '{"errorMessage":"Error","errorType":"ErrInvalidRegSourceId","stackTrace":["e.getErrorObj (/var//index.js:1:123)","E.t.(anonymous function)"]}'
      }
      mockLambda.call.mockReturnValueOnce(expectedError)
    })
    it('should return 404 response', async () => {
      try {
        await strategy.dispatch(event)
        throw new Error('Should not be hitting this error!')
      } catch (err) {
        expect(err.statusCode).toEqual(404)
      }
    })
  })
  describe('when no registration source Id is passed', () => {
    beforeEach(() => {
      event = {}
    })
    it('should return 400 response', async () => {
      try {
        await strategy.dispatch(event)
        throw new Error('Should not be hitting this error!')
      } catch (err) {
        expect(err.statusCode).toEqual(400)
      }
    })
  })
})
