import CreateRegSourceStrategy from './CreateRegSourceStrategy'
import {BRANDS, REGISTRATION_SOURCE_TYPE, END_OF_PROCESS_TYPE} from '../../../models/api/enumConstants'

describe('CreateRegSourceStrategy', () => {
  let strategy, event, mockEnvVars, mockLambda, expectedRegSourceFunction
  strategy = new CreateRegSourceStrategy()
  beforeEach(() => {
    expectedRegSourceFunction = 'createRegSource'
    mockEnvVars = {
      get: jest.fn(key => {
        if (key === 'createRegSourceFunction') return expectedRegSourceFunction
      })
    }
    mockLambda = {
      call: jest.fn(() => Promise.resolve())
    }
    strategy = new CreateRegSourceStrategy(mockEnvVars, mockLambda)
  })
  describe('when dispatch() is called', () => {
    beforeEach(() => {
      event = {}
      event.body = JSON.stringify({
        data: {
          name: 'Registration Source 1',
          brand: BRANDS[0],
          category: 'Test Category',
          subCategory: 'test',
          registrationSourceType: REGISTRATION_SOURCE_TYPE.ONSITE,
          endOfProcessType: END_OF_PROCESS_TYPE.EO,
          active: true
        }
      })
    })
    it('should invoke the correct lambda', async () => {
      await strategy.dispatch(event)
      expect(mockEnvVars.get).toHaveBeenCalled()
      expect(mockEnvVars.get).toHaveBeenCalledWith('createRegSourceFunction')
      expect(mockLambda.call).toHaveBeenCalled()
      expect(mockLambda.call.mock.calls[0][0]).toEqual('invoke')
      expect(mockLambda.call.mock.calls[0][1].FunctionName).toEqual(expectedRegSourceFunction)
    })
  })
  describe('when event is passed with a body', () => {
    beforeEach(() => {
      event = {}
      event.body = JSON.stringify({
        data: {
          name: 'Registration Source 1',
          brand: BRANDS[1],
          category: 'bhg',
          subCategory: 'test',
          registrationSourceType: REGISTRATION_SOURCE_TYPE.ONSITE,
          endOfProcessType: END_OF_PROCESS_TYPE.EO,
          active: true
        }
      })
      mockLambda.call.mockReturnValueOnce({
        Payload: JSON.stringify({ data: event.body })
      })
    })
    it('should return 201 response', async () => {
      const response = await strategy.dispatch(event)
      expect(response).toBeDefined()
      expect(response.statusCode).toEqual(201)
      expect(response.body).toBeDefined()
    })
  })
  describe('when event is passed without a body', () => {
    beforeEach(() => {
      event = {}
    })
    it('should return 400 statusCode', async () => {
      try {
        await strategy.dispatch(event)
      } catch (err) {
        expect(err).toBeDefined()
        expect(err.statusCode).toEqual(400)
      }
    })
  })
  describe('when event does not include a name', () => {
    event = {}
    event.body = JSON.stringify({
      data: {
        brand: BRANDS[1],
        category: 'bhg',
        subCategory: 'test',
        registrationSourceType: REGISTRATION_SOURCE_TYPE.ONSITE,
        endOfProcessType: END_OF_PROCESS_TYPE.EO,
        active: true
      }
    })
    it('should return a 400 error', async () => {
      try {
        await strategy.dispatch(event)
      } catch (err) {
        expect(err).toBeDefined()
        expect(err.statusCode).toEqual(400)
      }
    })
  })
  describe('when the lambda returns an error', () => {
    beforeEach(() => {
      event = {}
      event.body = JSON.stringify({
        data: {
          name: 'Registration Source 1',
          brand: BRANDS[1],
          category: 'bhg',
          subCategory: 'test',
          registrationSourceType: REGISTRATION_SOURCE_TYPE.ONSITE,
          endOfProcessType: END_OF_PROCESS_TYPE.EO,
          active: true
        }
      })
      let expectedError = {
        StatusCode: 200,
        FunctionError: 'Handled',
        ExecutedVersion: '$LATEST',
        Payload: '{"errorMessage":"Error","errorType":"ErrCreatingRegSource","stackTrace":["e.getErrorObj (/var//index.js:1:123)","E.t.(anonymous function)"]}'
      }
      mockLambda.call.mockReturnValueOnce(expectedError)
    })
    it('should return a 400 error', async () => {
      try {
        await strategy.dispatch(event)
      } catch (err) {
        expect(err).toBeDefined()
        expect(err.statusCode).toEqual(400)
      }
    })
  })
  describe('when the lambda returns an internal server error', () => {
    beforeEach(() => {
      event = {}
      event.body = JSON.stringify({
        data: {
          name: 'Registration Source 1',
          brand: BRANDS[28],
          category: 'bhg',
          subCategory: 'test',
          registrationSourceType: REGISTRATION_SOURCE_TYPE.ONSITE,
          endOfProcessType: END_OF_PROCESS_TYPE.EO,
          active: true
        }
      })
      let expectedError = {
        StatusCode: 200,
        FunctionError: 'Handled',
        ExecutedVersion: '$LATEST',
        Payload: '{"errorMessage":"Error","errorType":"InternalServerError","stackTrace":["e.getErrorObj (/var//index.js:1:123)","E.t.(anonymous function)"]}'
      }
      mockLambda.call.mockReturnValueOnce(expectedError)
    })
    it('should return a 500 error', async () => {
      try {
        await strategy.dispatch(event)
      } catch (err) {
        expect(err).toBeDefined()
        expect(err.statusCode).toEqual(500)
      }
    })
  })
})
