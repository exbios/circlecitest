import { failure500, failure400, success } from '../../../lib/apigLambdaProxyHelper'
import { inject } from 'aurelia-dependency-injection'
import { isNil } from 'lodash'
import message from '../../../messagesStore'
import loggerFactory from '../../../lib/services/loggerFactory'

const logger = loggerFactory('FetchAllNewsletters')
const FILTERS = ['brandId']

@inject('EnvVarsManager', 'Lambda')
export default class FetchNewslettersStrategy {
  constructor(envVarsManager, lambda) {
    this._envVarsManager = envVarsManager
    this._lambda = lambda
  }

  async dispatch(event) {
    let filters = {}
    let index = 0
    const queryParams = event.queryStringParameters

    if (!isNil(queryParams)) {
      // Creating a new array with keys in lowerCase from queryParams
      const keysInLowerCase = Object.keys(queryParams)

      // Validating if exist a valid queryParameter in the event
      for (;keysInLowerCase.length > index; index++) {
        let lowerKey = keysInLowerCase[index]
        let foundKey = FILTERS.find(function (element) {
          return element.toLowerCase() === lowerKey.toLowerCase()
        })
        if (isNil(foundKey)) {
          throw failure400(message.ERR_BAD_QUERY_PARAMETER)
        }
        filters[foundKey] = queryParams[lowerKey].toLowerCase()
      }
    }

    const params = {
      FunctionName: this._envVarsManager.get('fetchAllNewslettersFunction'),
      Payload: JSON.stringify(filters)
    }

    const response = await this._lambda.call('invoke', params)

    if (response && response.Payload) {
      const itemsFromLambda = JSON.parse(response.Payload)

      if (response.FunctionError) {
        const errorObj = new Error(response.Payload)
        logger.error(`An error was returned to dispatcher: ${JSON.stringify(response.Payload)}`)
        throw failure500(errorObj)
      }
      return success(itemsFromLambda) // Status 200
    }
  }
}
