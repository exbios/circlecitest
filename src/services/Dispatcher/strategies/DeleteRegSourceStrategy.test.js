import DeleteRegSourceStrategy from './DeleteRegSourceStrategy'

describe('DeleteRegSourceStrategy', () => {
  let strategy, event, mockEnvVars, mockLambda, expectedRegSourceFunction
  strategy = new DeleteRegSourceStrategy()
  beforeEach(() => {
    expectedRegSourceFunction = 'deleteRegSource'
    mockEnvVars = {
      get: jest.fn(key => {
        if (key === 'deleteRegSourceFunction') return expectedRegSourceFunction
      })
    }
    mockLambda = {
      call: jest.fn(() => Promise.resolve())
    }
    strategy = new DeleteRegSourceStrategy(mockEnvVars, mockLambda)
  })
  describe('when event is passed without a path parameter', () => {
    beforeEach(() => {
      event = {
        pathParameters: {}
      }
    })
    it('should return 400 statusCode', async () => {
      try {
        await strategy.dispatch(event)
      } catch (err) {
        var errorBody = JSON.parse(err.body)
        expect(err).toBeDefined()
        expect(err.statusCode).toEqual(400)
        expect(errorBody.errors[0].source.context.message).toEqual('Missing path params')
      }
    })
  })
  describe('when dispatch() is called', () => {
    beforeEach(() => {
      event = {
        pathParameters: { regsourceId: '0c1f19' }
      }
    })
    it('should invoke the correct lambda', async () => {
      await strategy.dispatch(event)
      expect(mockEnvVars.get).toHaveBeenCalled()
      expect(mockEnvVars.get).toHaveBeenCalledWith('deleteRegSourceFunction')
      expect(mockLambda.call).toHaveBeenCalled()
      expect(mockLambda.call.mock.calls[0][0]).toEqual('invoke')
      expect(mockLambda.call.mock.calls[0][1].FunctionName).toEqual(expectedRegSourceFunction)
    })
  })
  describe('when dispatch() is called', () => {
    beforeEach(() => {
      event = {
        pathParameters: {regsourceId: '0c1f19'}
      }
      mockLambda.call.mockReturnValueOnce({
        Payload: JSON.stringify({})
      })
    })
    it('should return 204 response', async () => {
      const response = await strategy.dispatch(event)
      expect(response.body).toEqual(JSON.stringify({}))
    })
  })
  describe('when event is passed without a path parameter', () => {
    beforeEach(() => {
      event = {
        pathParameters: {newsletterId: undefined}
      }
    })
    it('should return 400 statusCode', async () => {
      try {
        await strategy.dispatch(event)
      } catch (err) {
        var errorBody = JSON.parse(err.body)
        expect(err).toBeDefined()
        expect(err.statusCode).toEqual(400)
        expect(errorBody.errors[0].source.context.message).toEqual('Missing path params')
      }
    })
  })
  describe('when the lambda returns an error', () => {
    beforeEach(() => {
      event = {
        pathParameters: {regsourceId: '0c1f19'}
      }
      let expectedError = {
        StatusCode: 200,
        FunctionError: 'Handled',
        ExecutedVersion: '$LATEST',
        Payload: '{"errorMessage":"Error updating: Error: ConditionalCheckFailedException:","errorType":"ErrDeletingRegSource","stackTrace":["n.getErrorObj"]}'
      }
      mockLambda.call.mockReturnValueOnce(expectedError)
    })
    it('should return a 409 error', async () => {
      try {
        await strategy.dispatch(event)
      } catch (err) {
        expect(err).toBeDefined()
        expect(err.statusCode).toEqual(409)
      }
    })
  })
  describe('when delete service returns an unexpected error', () => {
    beforeEach(() => {
      event = {
        pathParameters: {regsourceId: '0c1f19'}
      }
      let expectedError = {
        StatusCode: 200,
        FunctionError: 'Handled',
        ExecutedVersion: '$LATEST',
        Payload: '{"errorMessage":"Error","errorType":"InternalServerError","stackTrace":["e.getErrorObj (/var//index.js:1:123)","E.t.(anonymous function)"]}'
      }
      mockLambda.call.mockReturnValueOnce(expectedError)
    })
    it('should return a 500 Internal Server Error', async () => {
      try {
        await strategy.dispatch(event)
      } catch (err) {
        expect(err).toBeDefined()
        expect(err.statusCode).toEqual(500)
      }
    })
  })
})
