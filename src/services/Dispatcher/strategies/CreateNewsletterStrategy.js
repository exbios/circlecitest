import { inject } from 'aurelia-dependency-injection'
import { failure400, success201, failure500 } from '../../../lib/apigLambdaProxyHelper'
import { isValidSchema } from '../../../models/api/schemaValidator'
import { newsletterCreateRequest } from '../../../../documentation/models/newsletters'
import messages from '../../../messagesStore'
import { OWNER } from '../../../models/api/enumConstants'
import loggerFactory from '../../../lib/services/loggerFactory'
import { isNil } from 'lodash'

const logger = loggerFactory('CreateNewsletterStrategy')

@inject('EnvVarsManager', 'Lambda')
export default class CreateNewsletterStrategy {
  constructor(envVarsManager, lambda) {
    this._envVarsManager = envVarsManager
    this._lambda = lambda
  }
  async dispatch(event) {
    if (!event.body) {
      throw failure400(messages.ERR_MISSING_EVENT_BODY)
    }
    logger.info('Validating the schema')
    isValidSchema(newsletterCreateRequest, event.body)

    // Set default values
    const payload = JSON.parse(event.body)
    if (isNil(payload.data.active)) {
      payload.data.active = true
    }
    if (isNil(payload.data.owner)) {
      payload.data.owner = OWNER.DIGITAL
    }

    const params = {
      FunctionName: this._envVarsManager.get('createNewsletterFunction'),
      Payload: JSON.stringify(payload)
    }

    const response = await this._lambda.call('invoke', params)
    if (response && response.Payload) {
      if (response.FunctionError) {
        const errorObj = JSON.parse(response.Payload)
        logger.error(`An error was returned to dispatcher: ${JSON.stringify(errorObj)}`)
        switch (errorObj.errorType) {
          case 'ErrCreatingNewsletter':
            throw failure400(errorObj)
          default:
            throw failure500(errorObj)
        }
      }
      return success201(JSON.parse(response.Payload))
    }
  }
}
