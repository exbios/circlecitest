import { inject } from 'aurelia-dependency-injection'
import { isValidSchema } from '../../../models/api/schemaValidator'
import { regSourceUpdateRequest } from '../../../../documentation/models/regSource'
import messages from '../../../messagesStore'
import { failure400, failure409, failure500, success } from '../../../lib/apigLambdaProxyHelper'
import loggerFactory from '../../../lib/services/loggerFactory'
import { isNil } from 'lodash'

const logger = loggerFactory('UpdateRegSourceStrategy')

@inject('EnvVarsManager', 'Lambda')
export default class UpdateRegSourceStrategy {
  constructor(envVarsManager, lambda) {
    this._envVarsManager = envVarsManager
    this._lambda = lambda
  }
  async dispatch(event) {
    if (isNil(event.body)) {
      throw failure400(messages.ERR_MISSING_EVENT_BODY)
    }

    logger.info('Validating the schema')
    isValidSchema(regSourceUpdateRequest, event.body)

    const regSourceObj = JSON.parse(event.body).data
    regSourceObj.regSourceId = event.pathParameters.regsourceId

    const params = {
      FunctionName: this._envVarsManager.get('updateRegSourceFunction'),
      Payload: JSON.stringify(regSourceObj)
    }

    const response = await this._lambda.call('invoke', params)
    logger.debug('Response from service: ', JSON.stringify(response))
    if (response && response.Payload) {
      if (response.FunctionError) {
        const errorObj = JSON.parse(response.Payload)
        logger.error(`An error was returned to dispatcher: ${JSON.stringify(errorObj)}`)
        switch (errorObj.errorType) {
          case 'ErrUpdatingRegSource':
            throw failure409(errorObj)
          default:
            throw failure500(errorObj)
        }
      }
      return success(JSON.parse(response.Payload))
    }
  }
}
