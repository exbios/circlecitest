import { inject } from 'aurelia-dependency-injection'
import { failure500, success, failure400 } from '../../../lib/apigLambdaProxyHelper'
import loggerFactory from '../../../lib/services/loggerFactory'
import messages from '../../../messagesStore'
import { isNil } from 'lodash'

const logger = loggerFactory('FetchAllRegSources')
const FILTERS = ['brandId']

@inject('EnvVarsManager', 'Lambda')
export default class FetchRegSourcesStrategy {
  constructor(envVarsManager, lambda) {
    this._envVarsManager = envVarsManager
    this._lambda = lambda
  }

  async dispatch(event) {
    let filters = {}
    let index = 0
    const queryParams = event.queryStringParameters

    if (!isNil(queryParams)) {
      // Creating a new array with keys in lowerCase from queryParams
      const keysInLowerCase = Object.keys(queryParams)

      // Validating if exist a valid queryParameter in the event
      for (;keysInLowerCase.length > index; index++) {
        let lowerKey = keysInLowerCase[index]
        let foundKey = FILTERS.find(function (element) {
          return element.toLowerCase() === lowerKey.toLowerCase()
        })
        if (isNil(foundKey)) {
          throw failure400(messages.ERR_BAD_QUERY_PARAMETER)
        }
        filters[foundKey] = queryParams[lowerKey].toLowerCase()
      }
    }

    const params = {
      FunctionName: this._envVarsManager.get('fetchAllRegSourcesFunction'),
      Payload: JSON.stringify(filters)
    }

    const response = await this._lambda.call('invoke', params)
    if (response && response.Payload) {
      const itemsFromLambda = JSON.parse(response.Payload)

      if (response.FunctionError) {
        const errorObj = new Error(response.Payload)
        logger.error(`An error was returned to dispatcher: ${JSON.stringify(response.Payload)}`)
        throw failure500(errorObj)
      }
      return success(itemsFromLambda) // Status 200
    }
  }
}
