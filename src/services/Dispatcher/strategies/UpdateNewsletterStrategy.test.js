import UpdateNewsletterStrategy from './UpdateNewsletterStrategy'
import {OWNER, OBJECT_TYPE, BRANDS} from '../../../models/api/enumConstants'

describe('UpdateNewsletterStrategy', () => {
  let strategy, event, mockEnvVars, mockLambda, expectedNewsletterFunction, validNewsletter
  strategy = new UpdateNewsletterStrategy()
  beforeEach(() => {
    validNewsletter = {
      data: {
        title: 'Preparing for Pregnancy',
        description: 'Are you thinking of getting pregnant or already trying to conceive?',
        imageUrl: '/parents/images/newsletter/PFP_NLPage.jpg',
        brand: BRANDS[28],
        objectType: OBJECT_TYPE.NEWSLETTER,
        owner: OWNER.DIGITAL,
        active: true,
        tags: ['simlac']
      }
    }
    expectedNewsletterFunction = 'updateNewsletter'
    mockEnvVars = {
      get: jest.fn(key => {
        if (key === 'updateNewsletterFunction') return expectedNewsletterFunction
      })
    }
    mockLambda = {
      call: jest.fn(() => Promise.resolve())
    }
    strategy = new UpdateNewsletterStrategy(mockEnvVars, mockLambda)
  })
  describe('when dispatch() is called', () => {
    beforeEach(() => {
      event = {}
      event.pathParameters = {
        newsletterId: 'aa88yh'
      }
      event.body = JSON.stringify(validNewsletter)
    })
    it('should invoke the correct lambda', async () => {
      await strategy.dispatch(event)
      expect(mockEnvVars.get).toHaveBeenCalledWith('updateNewsletterFunction')
      expect(mockLambda.call).toHaveBeenCalled()
      expect(mockLambda.call.mock.calls[0][0]).toEqual('invoke')
      expect(mockLambda.call.mock.calls[0][1].FunctionName).toEqual(expectedNewsletterFunction)
    })
  })
  describe('when event is passed with a body', () => {
    beforeEach(() => {
      event = {}
      event.pathParameters = {
        newsletterId: 'aa88yhS'
      }
      event.body = JSON.stringify(validNewsletter)
      mockLambda.call.mockReturnValueOnce({
        Payload: JSON.stringify({ data: event.body })
      })
    })
    it('should return 200 response', async () => {
      const response = await strategy.dispatch(event)
      expect(response).toBeDefined()
      expect(response.statusCode).toEqual(200)
      expect(response.body).toBeDefined()
    })
  })
  describe('when event is passed without a body', () => {
    beforeEach(() => {
      event = {}
      event.pathParameters = {
        newsletterId: 'aa88yh'
      }
    })
    it('should return 400 statusCode', async () => {
      try {
        await strategy.dispatch(event)
      } catch (err) {
        expect(err).toBeDefined()
        expect(err.statusCode).toEqual(400)
      }
    })
  })
  describe('when the lambda returns ErrUpdatingNewsletter', () => {
    beforeEach(() => {
      event = {}
      event.pathParameters = {
        newsletterId: 'aa88yh'
      }
      event.body = JSON.stringify(validNewsletter)
      let expectedError = {
        StatusCode: 200,
        FunctionError: 'Handled',
        ExecutedVersion: '$LATEST',
        Payload: '{"errorMessage":"Error","errorType":"ErrUpdatingNewsletter","stackTrace":["e.getErrorObj (/var//index.js:1:123)","E.t.(anonymous function)"]}'
      }
      mockLambda.call.mockReturnValueOnce(expectedError)
    })
    it('should return a 409 error when a dynamoDB condition is not met', async () => {
      try {
        await strategy.dispatch(event)
      } catch (err) {
        expect(err).toBeDefined()
        expect(err.statusCode).toEqual(409)
      }
    })
  })
  describe('when the lambda returns another error', () => {
    beforeEach(() => {
      event = {}
      event.pathParameters = {
        newsletterId: 'aa88yh'
      }
      event.body = JSON.stringify(validNewsletter)
      let expectedError = {
        StatusCode: 200,
        FunctionError: 'Handled',
        ExecutedVersion: '$LATEST',
        Payload: '{"errorMessage":"Error","errorType":"InternalServerError","stackTrace":["e.getErrorObj (/var//index.js:1:123)","E.t.(anonymous function)"]}'
      }
      mockLambda.call.mockReturnValueOnce(expectedError)
    })
    it('should return a 500 error', async () => {
      try {
        await strategy.dispatch(event)
      } catch (err) {
        expect(err).toBeDefined()
        expect(err.statusCode).toEqual(500)
      }
    })
  })
})
