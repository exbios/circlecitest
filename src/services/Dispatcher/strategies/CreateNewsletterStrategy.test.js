import CreateNewsletterStrategy from './CreateNewsletterStrategy'
import {OWNER, OBJECT_TYPE, BRANDS} from '../../../models/api/enumConstants'

describe('CreateNewsletterStrategy', () => {
  let strategy, event, mockEnvVars, mockLambda, expectedNewsletterFunction, validNewsletter
  strategy = new CreateNewsletterStrategy()
  beforeEach(() => {
    validNewsletter = {
      data: {
        title: 'Preparing for Pregnancy',
        description: 'Are you thinking of getting pregnant or already trying to conceive?',
        imageUrl: '/parents/images/newsletter/PFP_NLPage.jpg',
        brand: BRANDS[12],
        objectType: OBJECT_TYPE.NEWSLETTER,
        owner: OWNER.DIGITAL,
        active: true,
        tags: ['budget recipes']
      }
    }
    expectedNewsletterFunction = 'createNewsletter'
    mockEnvVars = {
      get: jest.fn(key => {
        if (key === 'createNewsletterFunction') return expectedNewsletterFunction
      })
    }
    mockLambda = {
      call: jest.fn(() => Promise.resolve())
    }
    strategy = new CreateNewsletterStrategy(mockEnvVars, mockLambda)
  })
  describe('when dispatch() is called', () => {
    beforeEach(() => {
      event = {}
      event.body = JSON.stringify(validNewsletter)
    })
    it('should invoke the correct lambda', async () => {
      await strategy.dispatch(event)
      expect(mockEnvVars.get).toHaveBeenCalled()
      expect(mockEnvVars.get).toHaveBeenCalledWith('createNewsletterFunction')
      expect(mockLambda.call).toHaveBeenCalled()
      expect(mockLambda.call.mock.calls[0][0]).toEqual('invoke')
      expect(mockLambda.call.mock.calls[0][1].FunctionName).toEqual(expectedNewsletterFunction)
    })
  })
  describe('when event is passed with a body', () => {
    beforeEach(() => {
      event = {}
      event.body = JSON.stringify(validNewsletter)
      mockLambda.call.mockReturnValueOnce({
        Payload: JSON.stringify({ data: event.body })
      })
    })
    it('should return 201 response', async () => {
      const response = await strategy.dispatch(event)
      expect(response).toBeDefined()
      expect(response.statusCode).toEqual(201)
      expect(response.body).toBeDefined()
    })
  })
  describe('when event is passed without a body', () => {
    beforeEach(() => {
      event = {}
    })
    it('should return 400 statusCode', async () => {
      try {
        await strategy.dispatch(event)
      } catch (err) {
        expect(err).toBeDefined()
        expect(err.statusCode).toEqual(400)
      }
    })
  })
  describe('when event does not include required attributes', () => {
    let noNameNewsletter
    beforeEach(() => {
      noNameNewsletter = validNewsletter
      delete noNameNewsletter.data.brand
      delete noNameNewsletter.data.objectType
      delete noNameNewsletter.data.owner
      delete noNameNewsletter.data.active
      event = {}
      event.body = JSON.stringify(noNameNewsletter)
    })
    it('should return a 400 error', async () => {
      try {
        await strategy.dispatch(event)
      } catch (err) {
        expect(err).toBeDefined()
        expect(err.statusCode).toEqual(400)
      }
    })
  })
  describe('when the lambda returns ErrCreatingNewsletter', () => {
    beforeEach(() => {
      event = {}
      event.body = JSON.stringify(validNewsletter)
      let expectedError = {
        StatusCode: 200,
        FunctionError: 'Handled',
        ExecutedVersion: '$LATEST',
        Payload: '{"errorMessage":"Error","errorType":"ErrCreatingNewsletter","stackTrace":["e.getErrorObj (/var//index.js:1:123)","E.t.(anonymous function)"]}'
      }
      mockLambda.call.mockReturnValueOnce(expectedError)
    })
    it('should return a 400 error', async () => {
      try {
        await strategy.dispatch(event)
      } catch (err) {
        expect(err).toBeDefined()
        expect(err.statusCode).toEqual(400)
      }
    })
  })
  describe('when the lambda returns another error', () => {
    beforeEach(() => {
      event = {}
      event.body = JSON.stringify(validNewsletter)
      let expectedError = {
        StatusCode: 200,
        FunctionError: 'Handled',
        ExecutedVersion: '$LATEST',
        Payload: '{"errorMessage":"Error","errorType":"InternalServerError","stackTrace":["e.getErrorObj (/var//index.js:1:123)","E.t.(anonymous function)"]}'
      }
      mockLambda.call.mockReturnValueOnce(expectedError)
    })
    it('should return a 500 error', async () => {
      try {
        await strategy.dispatch(event)
      } catch (err) {
        expect(err).toBeDefined()
        expect(err.statusCode).toEqual(500)
      }
    })
  })
})
