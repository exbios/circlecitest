import FetchRegSourcesStrategy from './FetchRegSourcesStrategy'

describe('FetchRegSourcesStrategy', () => {
  let strategy, mockEnvVars, mockLambda, expectedRegSourceFunction, expectedEvent
  strategy = new FetchRegSourcesStrategy()
  beforeEach(() => {
    expectedRegSourceFunction = 'fetchAllRegSourcesFunction'
    expectedEvent = {
      resource: '/regsources',
      path: '/regsources',
      httpMethod: 'GET',
      queryStringParameters: {
        brandId: 'cols'
      },
      pathParameters: null,
      stageVariables: null,
      body: null
    }
    mockEnvVars = {
      get: jest.fn(key => {
        if (key === 'fetchAllRegSourcesFunction') return expectedRegSourceFunction
      })
    }
    mockLambda = {
      call: jest.fn(() => Promise.resolve())
    }
    strategy = new FetchRegSourcesStrategy(mockEnvVars, mockLambda)
  })

  describe('when handler executed correctly', () => {
    let expectedResponse
    let emptyEvent = {}
    beforeEach(() => {
      expectedResponse = {
        Payload: `{
          "data": [
            {
              "external_id": "123",
              "name": "Extended Registration Source",
              "brand": "bhg",
              "category": "scholar",
              "sub_category": "bookworm",
              "registration_source_targets": [
                {
                  "type": "Newsletters",
                  "id": "aa88yh"
                }
              ],
              "title": "Join @griculture.com today and download your FREE Ageless Iron screensavers!",
              "redirect_url": "/bhg/file.jhtml?item=/contests/IrelandSweeps/IrelandSweeps_Thanks&temp=yes",
              "mobile_creative_url": "/bhg/images/marketing/registration/regHeaders/holiday/640x960M_10287_GG_PaintClr_v2.jpg",
              "mobile_creative_alt_text": "Booyah",
              "provider": "Meredith",
              "registration_source_type": "onsite",
              "paid_flag": false,
              "end_of_process_type": "Custom URL"
            }
          ]
        }`
      }
      mockLambda.call.mockReturnValueOnce(expectedResponse)
    })
    it('should return a 200 success', async () => {
      const response = await strategy.dispatch(emptyEvent)
      expect(response).toBeDefined()
      expect(response.statusCode).toEqual(200)
      expect(response.body).toBeDefined()
    })
  })

  describe('when dispatch() is called', () => {
    it('should invoke the correct lambda', async () => {
      await strategy.dispatch(expectedEvent)
      expect(mockEnvVars.get).toHaveBeenCalled()
      expect(mockEnvVars.get).toHaveBeenCalledWith('fetchAllRegSourcesFunction')
      expect(mockLambda.call).toHaveBeenCalled()
      expect(mockLambda.call.mock.calls[0][0]).toEqual('invoke')
      expect(mockLambda.call.mock.calls[0][1].FunctionName).toEqual(expectedRegSourceFunction)
      expect(mockLambda.call.mock.calls[0][1].Payload).toEqual(JSON.stringify(expectedEvent.queryStringParameters))
    })
  })

  describe('when handler return a empty array', () => {
    let expectedResponse
    beforeEach(() => {
      expectedResponse = {
        Payload: {
          data: []
        }
      }
      mockLambda.call.mockReturnValueOnce({ Payload: JSON.stringify(expectedResponse.Payload) })
    })
    it('should return a 400 error', async () => {
      try {
        await strategy.dispatch(expectedEvent)
      } catch (err) {
        expect(err).toBeDefined()
        expect(err.statusCode).toEqual(404)
      }
    })
  })

  describe('when called with missing or bad queryParameter', () => {
    let event
    beforeEach(() => {
      event = {
        queryStringParameters: {
          brand: 'cols'
        }
      }
    })
    it('should return a 400 error', async () => {
      try {
        await strategy.dispatch(event)
        throw new Error('Should not hit this error')
      } catch (err) {
        expect(err).toBeDefined()
        expect(err.statusCode).toEqual(400)
      }
    })
  })

  describe('when the lambda returns an error', () => {
    beforeEach(() => {
      let expectedError = {
        StatusCode: 200,
        FunctionError: 'Handled',
        ExecutedVersion: '$LATEST',
        Payload: '{"errorMessage":"Error","errorType":"ErrFetchingRegSources","stackTrace":["e.getErrorObj (/var//index.js:1:123)","E.t.(anonymous function)"]}'
      }
      mockLambda.call.mockReturnValueOnce(expectedError)
    })
    it('should return a 500 error', async () => {
      try {
        await strategy.dispatch(expectedEvent)
        throw new Error('Should not hit this error')
      } catch (err) {
        expect(err).toBeDefined()
        expect(err.statusCode).toEqual(500)
      }
    })
  })
})
