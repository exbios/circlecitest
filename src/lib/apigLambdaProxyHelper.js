/**
 * Provides helper functions to simplify integration with API Gateway requests/responses.
 */
import { get } from 'lodash'

export function extractBody(message) {
  const bodyString = get(message, 'body')
  if (bodyString) {
    return JSON.parse(bodyString)
  }
  return undefined
}

export function extractInternalError(err) {
  const message = get(err, 'stack')
  if (message instanceof Object) {
    return JSON.parse(message)
  }
  return message
}

export function extractMessage(err) {
  const message = get(err, 'message')
  if (message) {
    return JSON.parse(message)
  }
  return message
}

export function buildErrorModel(errorCode, errorMessage) {
  var errorResponse = {
    errorMessage: errorMessage
  }
  return buildResponse(errorCode, errorResponse, 'application/json')
}

export function buildExtendedErrorModel(err, errorCode, applicationErrorCode, errorType, errorMessage) {
  var customError = new Error(JSON.stringify(err))
  var errorResponse = {
    errors: [
      {
        code: applicationErrorCode,
        status: errorType,
        detail: errorMessage,
        source: {
          // trace: extractInternalError(customError),
          context: extractMessage(customError)
        }
      }
    ]
  }
  return buildResponse(errorCode, errorResponse, 'application/json')
}

export function extractPathParameters(message) {
  return get(message, 'pathParameters', {})
}
export function success(body) {
  return buildResponse(200, body)
}
export function success201(body) {
  return buildResponse(201, body)
}
export function success204() {
  return buildResponse(204, {})
}

/**
 * -------------------------------
 *  Reusable Responses (Based on HTTP status codes standards)
 * -------------------------------
 */
export function failure400(err) {
  return buildExtendedErrorModel(err, 400, 'ErrorBadRequest', '400_BAD_REQUEST', 'The request was invalid or cannot be otherwise served')
}
export function failure401(err) {
  return buildExtendedErrorModel(err, 401, 'ErrorUnauthorized', '401_UNAUTHORIZED', 'Authentication credentials were missing or incorrect.')
}
export function failure403(err) {
  return buildExtendedErrorModel(err, 403, 'ErrorForbidden', '403_FORBIDDEN', ' The request is understood, but it has been refused or access is not allowed.')
}
export function failure404() {
  return buildErrorModel(404, 'The requested resource was not found')
}
export function failure404TextPlain() {
  return buildResponse(404, 'Not Found', 'text/plain')
}
export function failure409(err) {
  return buildExtendedErrorModel(err, 409, 'ErrorConflict', '409_CONFLICT', err.message)
}
export function failure429() {
  return buildErrorModel(429, 'The request cannot be served due to the application’s rate limit having been exhausted for the resource.')
}
export function failure500(err) {
  return buildExtendedErrorModel(err, 500, 'ErrorInternalServerError', '500_INTERNAL_SERVER_ERROR', 'Something is broken')
}
export function failure503(err) {
  return buildExtendedErrorModel(err, 503, 'ErrorServiceUnavailable', '503_SERVICE_UNAVAILABLE', 'The server is up, but overloaded with requests. Try again later. ')
}
export function genericFailure(errorCode, errorMessage) {
  return buildErrorModel(errorCode || 500, errorMessage)
}

function buildResponse(statusCode, body, contentType) {
  return {
    statusCode: statusCode,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Content-Type': contentType
    },
    body: JSON.stringify(body)
  }
}
