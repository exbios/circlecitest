import { isFunction } from 'lodash'
import { init } from '../../lib/initFunction'
import { Container } from 'aurelia-dependency-injection'
import router from './'

jest.mock('../../lib/initFunction', () => {
  return { init: jest.fn(() => () => {}) }
})

describe('router', () => {
  it('should call init with RouterHandler', () => {
    expect(init.mock.calls[0][0]).toEqual('RouterHandler')
  })

  it('should register RouterHandler inside DI container', () => {
    const initContainerFunc = init.mock.calls[0][2]
    const container = new Container()
    initContainerFunc(container)
    expect(container.getResolver('RouterHandler')).toBeDefined()
  })

  it('should register needed services inside DI container', () => {
    const initContainerFunc = init.mock.calls[0][2]
    const container = new Container()
    initContainerFunc(container)
    expect(container._resolvers.size).toEqual(15)
    expect(container.getResolver('DispatcherService')).toBeDefined()
    expect(container.getResolver('CreateNewsletterStrategy')).toBeDefined()
    expect(container.getResolver('DeleteNewsletterStrategy')).toBeDefined()
    expect(container.getResolver('FetchNewsletterByIdStrategy')).toBeDefined()
    expect(container.getResolver('FetchNewslettersStrategy')).toBeDefined()
    expect(container.getResolver('UpdateNewsletterStrategy')).toBeDefined()
    expect(container.getResolver('CreateRegSourceStrategy')).toBeDefined()
    expect(container.getResolver('DeleteRegSourceStrategy')).toBeDefined()
    expect(container.getResolver('FetchRegSourcesStrategy')).toBeDefined()
    expect(container.getResolver('UpdateRegSourceStrategy')).toBeDefined()
    expect(container.getResolver('RouterHandler')).toBeDefined()
    expect(container.getResolver('EnvVarsManager')).toBeDefined()
    expect(container.getResolver('Lambda')).toBeDefined()
  })

  it('should return handler from init', () => {
    expect(isFunction(router)).toBeTruthy()
  })
})
