import RouterHandler from './RouterHandler'
import { success } from '../../lib/apigLambdaProxyHelper'

describe('RouterHandler', () => {
  let handler
  let mockDispatcherService
  let event, context

  beforeEach(() => {
    mockDispatcherService = {
      dispatch: jest.fn(() => Promise.resolve())
    }
    handler = new RouterHandler(mockDispatcherService)
    event = {
      httpMethod: 'GET',
      resource: '/to/my/resource',
      body: JSON.stringify({ prop1: 'prop1_value', props2: 'prop2_value' })
    }
    context = {}
  })

  describe('when incoming requests has content-type header requesting application/json', () => {
    let dispatchResponse
    beforeEach(() => {
      event.headers = {
        'Content-Type': 'application/json'
      }
      dispatchResponse = success({ body: 'res prop value' })
      mockDispatcherService.dispatch.mockReturnValueOnce(Promise.resolve(dispatchResponse))
    })

    it('should call dispatcher', async () => {
      await handler.execute(event, context)
      expect(mockDispatcherService.dispatch).toHaveBeenCalledWith(`${event.httpMethod}::${event.resource}`, event, context)
    })
  })

  describe('when dispatching is successful', () => {
    let dispatchResponse
    beforeEach(() => {
      dispatchResponse = success({ res: 'res prop value' })
      mockDispatcherService.dispatch.mockReturnValueOnce(Promise.resolve(dispatchResponse))
    })

    it('should return response from dispatcher', async () => {
      const response = await handler.execute(event, context)
      expect(response.statusCode).toEqual(200)
      expect(response.body).toBeDefined()
    })

    it('should always include the Access-Control-Allow-Origin response header', async () => {
      const response = await handler.execute(event, context)
      expect(response.statusCode).toEqual(200)
      expect(response.headers).toHaveProperty('Access-Control-Allow-Origin')
    })
  })
})
