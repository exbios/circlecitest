import { Container } from 'aurelia-dependency-injection'
import { init } from '../../lib/initFunction'
import RouterHandler from './RouterHandler'
import DispatcherService from '../../services/Dispatcher/DispatcherService'
import EnvVarsManager from '../../lib/services/EnvVarsManager'
import Lambda from '../../lib/services/aws/Lambda'
import CreateNewsletterStrategy from '../../services/Dispatcher/strategies/CreateNewsletterStrategy'
import DeleteNewsletterStrategy from '../../services/Dispatcher/strategies/DeleteNewsletterStrategy'
import FetchNewsletterByIdStrategy from '../../services/Dispatcher/strategies/FetchNewsletterByIdStrategy'
import FetchNewslettersStrategy from '../../services/Dispatcher/strategies/FetchNewslettersStrategy'
import UpdateNewsletterStrategy from '../../services/Dispatcher/strategies/UpdateNewsletterStrategy'

import CreateRegSourceStrategy from '../../services/Dispatcher/strategies/CreateRegSourceStrategy'
import DeleteRegSourceStrategy from '../../services/Dispatcher/strategies/DeleteRegSourceStrategy'
import FetchRegSourceByIdStrategy from '../../services/Dispatcher/strategies/FetchRegSourceByIdStrategy'
import FetchRegSourcesStrategy from '../../services/Dispatcher/strategies/FetchRegSourcesStrategy'
import UpdateRegSourceStrategy from '../../services/Dispatcher/strategies/UpdateRegSourceStrategy'

import CreateRegistrationStrategy from '../../services/Dispatcher/strategies/CreateRegistrationStrategy'

export default init(
  'RouterHandler',
  new Container(),
  container => {
    container.registerSingleton('DispatcherService', DispatcherService)
    container.registerSingleton('EnvVarsManager', EnvVarsManager)
    container.registerSingleton('Lambda', Lambda)
    // Newsletters Strategy definitions
    container.registerSingleton('CreateNewsletterStrategy', CreateNewsletterStrategy)
    container.registerSingleton('DeleteNewsletterStrategy', DeleteNewsletterStrategy)
    container.registerSingleton('FetchNewsletterByIdStrategy', FetchNewsletterByIdStrategy)
    container.registerSingleton('FetchNewslettersStrategy', FetchNewslettersStrategy)
    container.registerSingleton('UpdateNewsletterStrategy', UpdateNewsletterStrategy)
    // Reg Sources Strategy definitions
    container.registerSingleton('CreateRegSourceStrategy', CreateRegSourceStrategy)
    container.registerSingleton('DeleteRegSourceStrategy', DeleteRegSourceStrategy)
    container.registerSingleton('FetchRegSourceByIdStrategy', FetchRegSourceByIdStrategy)
    container.registerSingleton('FetchRegSourcesStrategy', FetchRegSourcesStrategy)
    container.registerSingleton('UpdateRegSourceStrategy', UpdateRegSourceStrategy)
    // Registration strategy definitions
    container.registerSingleton('CreateRegistrationStrategy', CreateRegistrationStrategy)
    container.registerSingleton('RouterHandler', RouterHandler)
  },
  []
)
