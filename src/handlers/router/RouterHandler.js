import { inject } from 'aurelia-dependency-injection'
import BaseHandler from '../../lib/handler/BaseHandler'

@inject('DispatcherService')
export default class RouterHandler extends BaseHandler {
  constructor(dispatcherService) {
    super()
    this._dispatcherService = dispatcherService
  }

  async _process(event, context) {
    this.logger.debug(`EVENT: ${JSON.stringify(event)}`)
    const route = this._mapPathAndMethod(event)
    this.logger.debug(`Dispatching route: ${route}`)
    try {
      const response = await this._dispatcherService.dispatch(route, event, context)
      this.logger.debug('Dispatcher response', response)
      return response
    } catch (err) {
      this.logger.error('Cannot dispatch request', err)
      return err
    }
  }

  _mapPathAndMethod(event) {
    return `${event.httpMethod}::${event.resource}`
  }
}
