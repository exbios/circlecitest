import { inject } from 'aurelia-dependency-injection'
import BaseHandler from '../../lib/handler/BaseHandler'
import { success, failure409, failure400, genericFailure } from '../../lib/apigLambdaProxyHelper'
const { get } = require('lodash')

@inject('EnvVarsManager')
export default class ServiceVersionStatusHandler extends BaseHandler {
  constructor(envVarsManager) {
    super()
    this.envVarsManager = envVarsManager
  }

  async _process(event, context) {
    const version = this.envVarsManager.get('serviceVersion')
    const versionNumber = get(event, 'pathParameters.version')
    const serviceName = get(event, 'pathParameters.serviceName')
    this.logger.info('Service version:', version)

    if (!versionNumber && !serviceName) {
      return failure400('Missing required path parameters')
    } else if (versionNumber > version) {
      return genericFailure(404, `Version: ${versionNumber} not managed by ${serviceName}`)
    } else if (versionNumber < version) {
      return failure409(`Deprecated service version in ${serviceName}`)
    }
    return success({ data: { serviceName: serviceName, managedVersion: version } })
  }
}
