import { Container } from 'aurelia-dependency-injection'
import { init } from '../../lib/initFunction'
import ServiceVersionStatusHandler from './ServiceVersionStatusHandler'

export default init('ServiceVersionStatusHandler', new Container(), container => {
  container.registerSingleton('ServiceVersionStatusHandler', ServiceVersionStatusHandler)
})
