import ServiceVersionStatusHandler from './ServiceVersionStatusHandler'
import { success } from '../../lib/apigLambdaProxyHelper'

describe('ServiceVersionStatusHandler', () => {
  let handler
  let mockEnvVarsManager
  let context, event

  beforeEach(() => {
    mockEnvVarsManager = {
      get: jest.fn()
    }
    event = {
      pathParameters: { serviceName: 'regSources', version: '0.0.1' }
    }
    handler = new ServiceVersionStatusHandler(mockEnvVarsManager)
    context = {}
  })

  it('should return a failure response for 409 scenario ', () => {
    const expectVersion = '1.0.0-20190314.local'
    mockEnvVarsManager.get.mockReturnValue(expectVersion)
    return handler.execute(event, context).then(response => {
      expect(event.pathParameters.serviceName).toEqual('regSources')
      expect(event.pathParameters.version).toEqual('0.0.1')
      expect(mockEnvVarsManager.get).toHaveBeenCalledWith('serviceVersion')
      expect(JSON.parse(response.body)).toHaveProperty('errors')
    })
  })

  it('should return successful response with the managed service version from environment variable', () => {
    const expectVersion = '1.0.0-20190314.local'
    mockEnvVarsManager.get.mockReturnValue(expectVersion)
    const jsonResponse = { data: { serviceName: 'regSources', managedVersion: expectVersion } }
    event.pathParameters = { serviceName: 'regSources', version: expectVersion }
    // mock successful response
    success(jsonResponse)
    return handler.execute(event, context).then(response => {
      expect(event.pathParameters.serviceName).toEqual('regSources')
      expect(event.pathParameters.version).toEqual(expectVersion)
      expect(mockEnvVarsManager.get).toHaveBeenCalledWith('serviceVersion')
      expect(JSON.parse(response.body)).toEqual(jsonResponse)
    })
  })
})
