import { Container } from 'aurelia-dependency-injection'
import { init } from '../../lib/initFunction'
import ServiceStatusHandler from './ServiceStatusHandler'

export default init('ServiceStatusHandler', new Container(), container => {
  container.registerSingleton('ServiceStatusHandler', ServiceStatusHandler)
})
