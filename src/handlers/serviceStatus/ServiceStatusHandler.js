import { inject } from 'aurelia-dependency-injection'
import BaseHandler from '../../lib/handler/BaseHandler'
import { success } from '../../lib/apigLambdaProxyHelper'

@inject('EnvVarsManager')
export default class ServiceStatusHandler extends BaseHandler {
  constructor(envVarsManager) {
    super()
    this.envVarsManager = envVarsManager
  }

  _process(event, context) {
    const version = this.envVarsManager.get('serviceVersion')
    this.logger.info('Service version:', version)
    return Promise.resolve(success({ data: { version: version } }))
  }
}
